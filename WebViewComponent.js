import React, { Component } from 'react';
import { WebView } from 'react-native-webview';
import {
  Text, View
} from 'react-native';
class MyWeb extends Component {
  render() {
    return (
      <View>        
        <WebView
          source={{uri: 'http://localhost:4200'}}
          style={{marginTop: 20}}
        />
        <Text style={footer}>Todo Test</Text>
      </View>      
    );
  }
}

const footer = {
  color: '#666666',
  fontSize: 12,
  fontWeight: '600',
  padding: 4,
  paddingRight: 12,
  textAlign: 'center',
};

export default MyWeb;